(function($) {
    $.fn.email_val = function() {
        this.each(function() {
            $(this).keyup(function() {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                
                if (re.test($(this).val())) {
                  $(this).css({"border-color" : "rgba(0, 170, 0, 1)", "box-shadow" : "0 0 5px rgba(0, 170, 0, 1)"});
                }
                else {
                $(this).css({"border-color" : "red", "box-shadow" : "0 0 5px rgba(255, 0, 0, 1)"});
                }
                  
            });
            $(this).change(function() {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                
                if (re.test($(this).val())) {
                  $(this).css({"border-color" : "rgba(0, 170, 0, 1)", "box-shadow" : "0 0 5px rgba(0, 170, 0, 1)"});
                }
                else {
                  $(this).css({"border-color" : "red", "box-shadow" : "0 0 5px rgba(255, 0, 0, 1)"});
                
               }   
            });
        });
    }
}(jQuery));


(function($) {
    $.fn.date_val = function() {
        this.each(function() {
            $(this).keyup(function() {
                var re = /^\d{2}\/\d{2}\/\d{4}$/;
                
                if (re.test($(this).val())) {
                  $(this).css({"border-color" : "rgba(0, 170, 0, 1)", "box-shadow" : "0 0 5px rgba(0, 170, 0, 1)"});
                }
                else {
                $(this).css({"border-color" : "red", "box-shadow" : "0 0 5px rgba(255, 0, 0, 1)"});
                }
                  
            });
            $(this).change(function() {
                var re = /^\d{2}\/\d{2}\/\d{4}$/;
                
                if (re.test($(this).val())) {
                  $(this).css({"border-color" : "rgba(0, 170, 0, 1)", "box-shadow" : "0 0 5px rgba(0, 170, 0, 1)"});
                }
                else {
                  $(this).css({"border-color" : "red", "box-shadow" : "0 0 5px rgba(255, 0, 0, 1)"});
                
               }   
            });
        });
    }
}(jQuery));
