module CurrentCart
  extend ActiveSupport::Concern
  
  private 
  
    def set_cart
      @cart = Cart.find(session[:cart_id])
      rescue ActiveRecord::RecordNotFound
      @cart = Cart.create 
      session[:cart_id] = @cart.id
      
    end
    
    def cart_access
        cart = Cart.find(session[:cart_id])
        show_cart = Cart.find(params[:id])
        if cart != show_cart
            redirect_to store_url
        end
    end

end